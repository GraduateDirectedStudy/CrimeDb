Download dataset from the given link- https://data.lacity.org/A-Safe-City/LAPD-Crime-and-Collision-Raw-Data-2014/eta5-h8qx
Download and install Azure Management studio - http://www.cerebrata.com/products/azure-management-studio/introduction
Log in Microsoft's Azure account and create storage from left navigation. Once storage is created, create HDINSIGHT for the storage which was created.
After your HDINSIGHT is created, open azure management studio and connect to your cloud storage by using the storage name and access key(access key can be obtained from azure portal)
Once storage get's connected , HDINSIGHT for that storage will be seen in azure management studio.
Create a folder in desired path and upload your dataset(.csv file)
Open Hive editor from Azure portal and create an external table for your dataset.
Write hive queries to analyze the data.